5Host 1.1
====================



## What is 5Host?
5Host is a file sharing site that allows anyone (and [anything](http://i1.kym-cdn.com/photos/images/newsfeed/000/427/566/b67.jpeg)) to upload and share content. Vote on the stuff you like, or not. It doesn't concern us.


## What makes 5Host special?
5Host merges the Anonymity and beauty of Imageboards with the file sharing capabilites of cloud storage. Users can anonymously upload files and share them online with anyone. 5Host also generates a sharable link and posts the file for public view.

## How can I contribute?
This repository is open for anyone to contribute. We will accept most, if not all merge requests as long as it does not break functionality. You will be added to `CONTRIBUTORS` if your merge request is accepted.

## Installing
Installing 5Host is simple:
* Prepare a web server that uses MySQL and PHP. LEMP or LAMP will work perfectly (just enable PHP in Nginx settings)
* Clone this repository and place the files in your web server's content folder.
* Create a schema in MySQL and import the database: `mysql -u <Your Username> -p <Database Name> < 5host.sql`
* **Make sure uploads are enabled in your server config.**
* In config.php, adjust `$host`, `$database`, `$username` and `$password` to match your server settings.
* Connect to your 5Host instance to check if you can see the test entries. If you can, Delete the test database entries from the database. If not, check your settings.
* Enjoy! Let us know via liam@ceteri.org about your install and we'll add you to the list of servers!

**CeteriMedia, LLC can not be held responsible for any files hosted on 5Host and do not have any control over community installations of 5Host. It is your responsibility to moderate uploaded files and remove any that break local laws.**

## Live Instances of 5Host
* None yet!

### Further information

5Host was officially incepted by Liam Newman and Ian Murray, who are part of [CeteriMedia](https://ceteri.org). As of the terms given within the MIT License, you may not claim this work or any similar works (e.g. community contributions) as your own.

Bootstrap, Twitter Bootstrap, the Bootstrap logo, or any similar works are licensed under the MIT License, and are licensed to [@mdo](https://twitter.com/mdo) and [@fat](https://twitter.com/fat).
